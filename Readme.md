Test aracının çalışması için testin çalıştırılacağı bilgisayarda öncelikle aşağıdaki koşullar sağlanmalıdır:

	1. Java 8 veya üstü kurulu olmalıdır.
	2. Apache Maven kurulu olmalıdır.

Testi çalıştırıp rapor almak için aşağıdaki adımlar takip edilir.

	1. Console'dan projenin olduğu klasöre gidilir.

	   Örnek: cd D:\projects\turkcell-poc-test
	
	2. Console'a "mvn clean verify" yazıp çalıştırarak test programı çalıştırılır.

	3. Test sonucunda çıkan raporu görmek için proje içinde aşağıda belirtilen gidilir.

	  Örnek: D:\projects\turkcell-poc-test\target\site\serenity\index.html