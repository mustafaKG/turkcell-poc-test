package common.helpers;

import screenplay.model.AdressInformation;
import screenplay.model.CustomerInformation;
import screenplay.user_interfaces.ApplicationPages;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class LogInformations {

    private static CustomerInformation getCustomer(boolean old) {
        String text = ApplicationPages.LogPage.updateResponse.resolveFor(theActorInTheSpotlight()).getText();
        text = text.replace('(', ',');
        text = text.replace(')', ',');
        text = text.replace('\n', ',');
        text = text.replaceAll(",+", ",");
        if (old)
            text = text.substring(text.indexOf("oldCustomer") + 12, text.indexOf("newCustomer"));
        else
            text = text.substring(text.indexOf("newCustomer"));

        return parseCustomer(text);
    }

    private static CustomerInformation parseCustomer(String customerInfo) {
        CustomerInformation customerInformation = new CustomerInformation();
        AdressInformation adressInformation = new AdressInformation();
        customerInformation.setMail(customerInfo.substring(customerInfo.indexOf("email") + 6, customerInfo.indexOf("email") + customerInfo.substring(customerInfo.indexOf("email")).indexOf(',')));
        customerInformation.setPhone(customerInfo.substring(customerInfo.indexOf("phone") + 6, customerInfo.indexOf("phone") + customerInfo.substring(customerInfo.indexOf("phone")).indexOf(',')));
        adressInformation.setCountry(customerInfo.substring(customerInfo.indexOf("country") + 8, customerInfo.indexOf("country") + customerInfo.substring(customerInfo.indexOf("country")).indexOf(',')));
        adressInformation.setCity(customerInfo.substring(customerInfo.indexOf("city") + 5, customerInfo.indexOf("city") + customerInfo.substring(customerInfo.indexOf("city")).indexOf(',')));
        adressInformation.setStreet(customerInfo.substring(customerInfo.indexOf("street") + 7, customerInfo.indexOf("street") + customerInfo.substring(customerInfo.indexOf("street")).indexOf(',')));
        adressInformation.setPostCode(customerInfo.substring(customerInfo.indexOf("postCode") + 9, customerInfo.indexOf("postCode") + customerInfo.substring(customerInfo.indexOf("postCode")).indexOf(',')));
        customerInformation.setAddress(adressInformation);
        return customerInformation;
    }

    public static CustomerInformation getOldCustomer() {
        return getCustomer(true);
    }

    public static CustomerInformation getNewCustomer() {
        return getCustomer(false);
    }
}
