package common.helpers;

import com.google.gson.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.SSLConfig;
import com.jayway.restassured.response.Response;
import common.utils.GetBaseConf;
import screenplay.model.CatalogInformation;
import screenplay.model.CustomerInformation;
import screenplay.model.OrderInformation;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class RestApiLog {

    public static List<OrderInformation> getOrderInformations(String tckn) {
        RestAssured.baseURI = GetBaseConf.getProperty("MWURL");
        Response response = given()
                .config(RestAssured.config().sslConfig(new SSLConfig().allowAllHostnames()))
                .header("Content-Type", "application/json")
                .when()
                .get("/order-service/api/order/" + tckn)
                .then()
                .extract().response();

        JsonParser parser = new JsonParser();
        JsonArray jsonArray = parser.parse(response.getBody().asString()).getAsJsonArray();
        return getOrders(jsonArray);
    }

    private static List<OrderInformation> getOrders(JsonArray jsonElements) {
        Gson gson = new Gson();
        List<OrderInformation> orderInformations = new ArrayList<>();
        for (JsonElement element : jsonElements) {
            OrderInformation orderInformation = gson.fromJson(element, OrderInformation.class);
            orderInformations.add(orderInformation);
        }
        return orderInformations;
    }

    public static CustomerInformation getCustomerInformation(String tckn) {
        RestAssured.baseURI = GetBaseConf.getProperty("MWURL");
        Response response = given()
                .config(RestAssured.config().sslConfig(new SSLConfig().allowAllHostnames()))
                .header("Content-Type", "application/json")
                .when()
                .post("converter-service/api/customer/" + tckn)
                .then()
                .extract().response();
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(response.getBody().asString()).getAsJsonObject();
        return gson.fromJson(jsonObject, CustomerInformation.class);
    }

    public static CatalogInformation getCatalogInformation(String id) {
        RestAssured.baseURI = GetBaseConf.getProperty("MWURL");
        Response response = given()
                .config(RestAssured.config().sslConfig(new SSLConfig().allowAllHostnames()))
                .header("Content-Type", "application/json")
                .when()
                .get("converter-service/api/catalog/" + id)
                .then()
                .extract().response();
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(response.getBody().asString()).getAsJsonObject();
        return gson.fromJson(jsonObject, CatalogInformation.class);
    }

}