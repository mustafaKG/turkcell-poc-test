package common.utils;

import common.framework.BaseConfiguration;

public class GetBaseConf {

	public static String getProperty(String property)
	{
		BaseConfiguration baseConfiguration = new BaseConfiguration();
		return baseConfiguration.getProperty(property);
	}
}
