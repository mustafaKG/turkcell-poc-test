package common.framework;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public class AutoDownloadedWebDriver implements DriverSource {
    public enum WebBrowsers {
        CHROME, CHROME_HEADLESS
    }

    private BaseConfiguration baseConfiguration = new BaseConfiguration();

    @Override
    public WebDriver newDriver() {
        WebBrowsers browserType = WebBrowsers.valueOf(baseConfiguration.getProperty("browser"));
        switch (browserType) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                DesiredCapabilities caps = new DesiredCapabilities();
                ChromeOptions opts = new ChromeOptions();
                opts.addArguments("–test-type", "–disable-extensions");
                caps.setCapability(ChromeOptions.CAPABILITY, opts);
                ChromeDriver driver = new ChromeDriver(caps);
                driver.manage().window().maximize();
                return driver;

            case CHROME_HEADLESS:
                WebDriverManager.chromedriver().setup();
                DesiredCapabilities _caps = new DesiredCapabilities();
                ChromeOptions _opts = new ChromeOptions();
                _opts.addArguments("--headless", "--disable-gpu", "--window-size=1920,1080","--ignore-certificate-errors", "–disable-extensions");
                _caps.setCapability(ChromeOptions.CAPABILITY, _opts);
                ChromeDriver _driver = new ChromeDriver(_caps);
                _driver.manage().window().maximize();
                return _driver;

            default:
                return null;
        }
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }
}
