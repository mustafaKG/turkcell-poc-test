package screenplay.user_interfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ApplicationPages extends PageObject {
    public static class OrderPage {
        public static Target product = Target.the("product").locatedBy("div.form-group label[for='product'] i");

        public static Target customer = Target.the("customer").locatedBy("div.form-group label[for='tckn'] i");

        public static Target onayButonu = Target.the("onayButonu").locatedBy("div button.ant-btn-primary");
    }
    public static class CustomerEditPage {
        public static Target tckn = Target.the("tckn").locatedBy("#tckn");

        public static Target name = Target.the("name").locatedBy("#cname");

        public static Target surname = Target.the("surname").locatedBy("#csurname");

        public static Target birthyear = Target.the("birthyear").locatedBy("#cbirthYear");

        public static Target mail = Target.the("mail").locatedBy("#cmail");

        public static Target phone = Target.the("phone").locatedBy("#phone");

        public static Target country = Target.the("country").locatedBy("#country");

        public static Target city = Target.the("city").locatedBy("#city");

        public static Target street = Target.the("street").locatedBy("#street");

        public static Target postCode = Target.the("postCode").locatedBy("#postCode");
    }
    public static class LogPage {
        public static Target methodName = Target.the("methodName").locatedBy("#methodName");

        public static Target updateResponse = Target.the("updateResponse").locatedBy(".ant-table-tbody td:nth-child(4)");
    }
}
