package screenplay.tasks;

import common.utils.GetBaseConf;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import screenplay.actions.Refresh;
import screenplay.view.ApplicationHomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class StartPoC2 implements Task {

    private ApplicationHomePage applicationHomePage;

    @Override
    public <T extends Actor> void performAs(T actor) {
        applicationHomePage.setDefaultBaseUrl(GetBaseConf.getProperty("homepage2"));
        actor.attemptsTo(
                Open.browserOn().the(applicationHomePage),
                Refresh.theBrowserSession()
        );
    }

    public static StartPoC2 withHomePage() {
        return instrumented(StartPoC2.class);
    }
}
