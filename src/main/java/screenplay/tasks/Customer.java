package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import screenplay.user_interfaces.ApplicationPages;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Customer implements Task {
    private String customer;

    public Customer(String customer) {
        this.customer = customer;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ApplicationPages.OrderPage.customer),
                Click.on("//li[@nz-option-li and contains(text(), '" + customer + "')]")
        );
    }

    public static Customer select(String customer) {
        return instrumented(Customer.class, customer);
    }
}
