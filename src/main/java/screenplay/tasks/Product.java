package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import screenplay.user_interfaces.ApplicationPages;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Product implements Task {
    private String product;

    public Product(String product) {
        this.product = product;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ApplicationPages.OrderPage.product),
                Click.on("//li[@nz-option-li and contains(text(), '" + product + "')]")
        );
    }

    public static Product select(String product) {
        return instrumented(Product.class, product);
    }
}
