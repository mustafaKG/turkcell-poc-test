package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import screenplay.model.CustomerInformation;
import screenplay.user_interfaces.ApplicationPages;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Edit implements Task {

    private CustomerInformation customerInformation;

    public Edit(CustomerInformation customerInformation) {
        this.customerInformation = customerInformation;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(customerInformation.getMail()).into(ApplicationPages.CustomerEditPage.mail),
                Enter.theValue(customerInformation.getPhone()).into(ApplicationPages.CustomerEditPage.phone),
                Enter.theValue(customerInformation.getAddress().getCountry()).into(ApplicationPages.CustomerEditPage.country),
                Enter.theValue(customerInformation.getAddress().getCity()).into(ApplicationPages.CustomerEditPage.city),
                Enter.theValue(customerInformation.getAddress().getStreet()).into(ApplicationPages.CustomerEditPage.street),
                Enter.theValue(customerInformation.getAddress().getPostCode()).into(ApplicationPages.CustomerEditPage.postCode)
        );
    }

    public static Edit customer(CustomerInformation customerInformation) {
        return instrumented(Edit.class, customerInformation);
    }
}
