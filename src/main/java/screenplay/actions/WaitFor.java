package screenplay.actions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class WaitFor implements Performable
{
    private long timeout;

    public WaitFor(long timeout) { this.timeout = timeout; }

    public <T extends Actor> void performAs(T actor) {
        try {Thread.sleep(timeout);}
        catch (InterruptedException e)
        {e.printStackTrace();}
    }

    public static WaitFor second(float scnd) {
        return instrumented(WaitFor.class, (long)(scnd*1000));
    }

    public static WaitFor millisecond(long millis) {
        return instrumented(WaitFor.class, millis);
    }
}
