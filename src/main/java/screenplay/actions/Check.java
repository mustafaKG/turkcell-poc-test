package screenplay.actions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.environment.ConfiguredEnvironment;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class Check{
    private Target target;
    private boolean condition;

    private Check(Target target) { this.target = target; }

    private Check(boolean condition) { this.condition = condition; }

    public static Check isTrue(boolean condition) { return new Check(condition); }

    public static Check ifElementExist(Target target) { return new Check(target); }

    @SafeVarargs
    public final <T extends  Performable> CheckAction then(T... performables) {
        return instrumented(CheckAction.class, condition, target, performables);
    }

}
