package screenplay.actions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class Hover implements Performable
{
    private Target target;

    public Hover(Target target) { this.target = target; }

    public static Hover over(Target target) { return instrumented(Hover.class, target); }

    @Override
    public <T extends Actor> void performAs(T actor) {
        WebDriver driver = BrowseTheWeb.as(theActorInTheSpotlight()).getDriver();
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(By.xpath(target.getCssOrXPathSelector()));
        action.moveToElement(element).perform();
    }
}
