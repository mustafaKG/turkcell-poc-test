package screenplay.actions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class Switch implements Performable
{
    private WebElement webElement;
    private static WebDriver driver = BrowseTheWeb.as(theActorInTheSpotlight()).getDriver();

    public Switch(WebElement webElement) { this.webElement = webElement; }
    public Switch() { }

    public static Switch toWebElement(WebElement webElement) { return instrumented(Switch.class, webElement); }

    public static Switch toFrame(int frameNo) {
        WebElement frame = driver.findElement(By.xpath("(//iframe)[" + frameNo + "]"));
        return instrumented(Switch.class, frame);
    }

    public static Switch toDefaultContent() { return instrumented(Switch.class); }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if (webElement != null)
            driver.switchTo().frame(webElement);
        else
            driver.switchTo().defaultContent();
    }
}
