package screenplay.actions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.environment.ConfiguredEnvironment;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CheckAction implements Performable {

    private Performable[] performables;
    private boolean condition;
    private Target target;
    WebDriver driver;

    @SafeVarargs
    public <T extends Performable> CheckAction(boolean condition, Target target, T... performables) {
        this.driver = BrowseTheWeb.as(theActorInTheSpotlight()).getDriver();
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
        this.condition = condition;
        this.target = target;
        this.performables = performables;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if (target != null) {
            if (target.getCssOrXPathSelector().contains("/"))
                condition = !driver.findElements(By.xpath(target.getCssOrXPathSelector())).isEmpty();
            else
                condition = !driver.findElements(By.cssSelector(target.getCssOrXPathSelector())).isEmpty();
        }
        if (condition)
            for (Performable task : performables)
                task.performAs(actor);
    }

    @SafeVarargs
    public final <T extends Performable> CheckAction otherwise(T... performables) {
        if (target != null) {
            if (target.getCssOrXPathSelector().contains("/"))
                condition = !driver.findElements(By.xpath(target.getCssOrXPathSelector())).isEmpty();
            else
                condition = !driver.findElements(By.cssSelector(target.getCssOrXPathSelector())).isEmpty();
        }
        if (!condition)
            return new CheckAction(true, null, performables);
        return this;
    }

    @Override
    public void finalize() {
        driver.manage().timeouts().implicitlyWait(ConfiguredEnvironment.getConfiguration().getElementTimeoutInSeconds(), TimeUnit.SECONDS);
    }
}
