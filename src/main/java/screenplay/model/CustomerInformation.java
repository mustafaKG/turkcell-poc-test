package screenplay.model;

import java.util.Objects;

public class CustomerInformation {
    private String name;
    private String surname;
    private String tckn;
    private String birthYear;
    private String mail;
    private String phone;
    private AdressInformation address;

    public CustomerInformation() {}

    public CustomerInformation(String mail, String phone, AdressInformation address) {
        this.mail = mail;
        this.phone = phone;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTckn() {
        return tckn;
    }

    public void setTckn(String tckn) {
        this.tckn = tckn;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public AdressInformation getAddress() {
        return address;
    }

    public void setAddress(AdressInformation address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "CustomerInformation{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", tckn='" + tckn + '\'' +
                ", birthYear='" + birthYear + '\'' +
                ", mail='" + mail + '\'' +
                ", phone='" + phone + '\'' +
                ", address=" + address +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerInformation)) return false;
        CustomerInformation that = (CustomerInformation) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(tckn, that.tckn) &&
                Objects.equals(birthYear, that.birthYear) &&
                Objects.equals(mail, that.mail) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(address, that.address);
    }

}
