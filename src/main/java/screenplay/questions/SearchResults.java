/*package screenplay.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Visibility;
import screenplay.user_interfaces.OrderSayfasi;

import static net.serenitybdd.screenplay.questions.ValueOf.the;

public class SearchResults implements Question<ElementAvailability> {

    @Override
    public ElementAvailability answeredBy(Actor actor) {
        return ElementAvailability.from(
                the(Visibility.of(OrderSayfasi.target)
                        .viewedBy(actor))
        );
    }

    public static Question<ElementAvailability> tarifeler() {
        return new SearchResults();
    }

}
*/