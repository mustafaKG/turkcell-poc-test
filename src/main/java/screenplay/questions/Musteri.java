package screenplay.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import screenplay.model.AdressInformation;
import screenplay.model.CustomerInformation;
import screenplay.user_interfaces.ApplicationPages;

public class Musteri implements Question<CustomerInformation> {

    @Override
    public CustomerInformation answeredBy(Actor actor) {
        CustomerInformation customerInformation = new CustomerInformation();
        AdressInformation adressInformation = new AdressInformation();
        customerInformation.setMail(ApplicationPages.CustomerEditPage.mail.resolveFor(actor).getValue());
        customerInformation.setPhone(ApplicationPages.CustomerEditPage.phone.resolveFor(actor).getValue());
        adressInformation.setCountry(ApplicationPages.CustomerEditPage.country.resolveFor(actor).getValue());
        adressInformation.setCity(ApplicationPages.CustomerEditPage.city.resolveFor(actor).getValue());
        adressInformation.setStreet(ApplicationPages.CustomerEditPage.street.resolveFor(actor).getValue());
        adressInformation.setPostCode(ApplicationPages.CustomerEditPage.postCode.resolveFor(actor).getValue());
        customerInformation.setAddress(adressInformation);
        return customerInformation;
    }

    public static Musteri information() {
        return new Musteri();
    }
}
