package features;

import common.helpers.LogInformations;
import common.helpers.RestApiLog;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import screenplay.actions.WaitFor;
import screenplay.model.AdressInformation;
import screenplay.model.CustomerInformation;
import screenplay.model.OrderInformation;
import screenplay.model.TestData;
import screenplay.questions.Musteri;
import screenplay.tasks.*;
import screenplay.user_interfaces.ApplicationPages;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class MainSteps {

    private Actor actor;
    private List<OrderInformation> orderInformations;
    private TestData testData;
    private CustomerInformation oldCustomerInformation;
    private CustomerInformation newCustomerInformation;

    @Before
    public void set_the_stage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^(.*) PoC anasayfasını açar$")
    public void pocAnasayfasiAcilir(String actorName) {
        theActorCalled(actorName).wasAbleTo(StartPoC.withHomePage());
        actor = theActorInTheSpotlight();
    }

    @Given("^(.*) PoC2 anasayfasını açar$")
    public void poc2AnasayfasiAcilir(String actorName) {
        theActorCalled(actorName).wasAbleTo(StartPoC2.withHomePage());
        actor = theActorInTheSpotlight();
    }

    @And("^(.*) Sekmesine Tıklanır$")
    public void sekmeyeTiklanir(String sekmeIsmi) {
        actor.attemptsTo(
                WaitFor.millisecond(1000),
                Click.on("//li[contains(@class,'ant-menu-item')]/a[contains(text(), '" + sekmeIsmi + "')]")
        );
    }

    @And("^(.*) Butonuna Tıklanır$")
    public void butonaTiklanir(String butonIsmi) {
        actor.attemptsTo(
                WaitFor.millisecond(1000),
                Click.on("//button[contains(@class, 'btn-outline-primary') and contains(text(), '" + butonIsmi + "')]")
        );
    }

    @And("^(.*) kimlik numaralı müşterinin önceki siparişleri tutulur$")
    public void tcknKimlikNumaraliMusterininOncekiSiparisleriTutulur(String tckn) {
        testData = new TestData();
        testData.setTckn(tckn);
        orderInformations = RestApiLog.getOrderInformations(tckn);
    }

    @And("^(.*) ürünü seçilir$")
    public void urunSecilir(String product) {
        testData.setProductName(product);
        actor.attemptsTo(Product.select(product));
    }

    @And("^(.*) müşterisi seçilir$")
    public void musteriSecilir(String customer) {
        testData.setCustomerName(customer);
        actor.attemptsTo(Customer.select(customer));
    }

    @And("^(.*) ait müşteri düzenlenir$")
    public void musteriDuzenlenir(String customer) {
        actor.attemptsTo(Click.on("//td[contains(text(), '" + customer + "')]/../td/button"));
    }

    @Then("^Müşterinin almış olduğu bu ürün verisi sorgulanır ve detayları dönülür$")
    public void musterininAlmisOlduguBuUrunVerisiSorgulanirVeDetaylariDonulur() {
        assertThat("Müşteriye sipariş eklenemedi", orderInformations.size() + 1  == RestApiLog.getOrderInformations(testData.getTckn()).size());

        orderInformations = RestApiLog.getOrderInformations(testData.getTckn());
        OrderInformation orderInformation = orderInformations.get(orderInformations.size() - 1);

        assertThat("Müşteriye eklenen sipariş farklı bir kimlik numarasına yazıldı",
                RestApiLog.getCustomerInformation(testData.getTckn()).getName(), equalTo(testData.getCustomerName()));

        assertThat("Müşteriye yanlış katalogtan bir sipariş eklendi",
                RestApiLog.getCatalogInformation(orderInformation.getProductId()).getName(), equalTo(testData.getProductName()));
    }

    @And("^Müşterinin (.*), (.*), (.*), (.*), (.*) ve posta kodu düzenlenir$")
    public void musterininAdresiDuzenlenir(String mail, String telefon, String ulke, String sehir, String cadde) {
        oldCustomerInformation = Musteri.information().answeredBy(actor);
        String postaKodu = String.valueOf(Long.parseLong(oldCustomerInformation.getAddress().getPostCode()) + 1);
        newCustomerInformation = new CustomerInformation(mail, telefon, new AdressInformation(ulke, sehir, cadde, postaKodu));
        actor.attemptsTo(Edit.customer(newCustomerInformation));
    }

    @And("^Çıkan uyarıda OK butonuna basılır$")
    public void cikanUyaridaOKButonunaBasilir() {
        actor.attemptsTo(Click.on(ApplicationPages.OrderPage.onayButonu));
    }

    @And("^Log sayfasından müşteri bilgilerinin düzenlendiği görülür$")
    public void logSayfasindanMusteriBilgilerininDuzenlendigiGorulur() {
        actor.attemptsTo(WaitFor.millisecond(2000));
        assertThat("Eski müşteri kaydı kontrolü: ",
                LogInformations.getOldCustomer(), equalTo(oldCustomerInformation));
        assertThat("Yeni müşteri kaydı kontrolü: ",
                LogInformations.getNewCustomer(), equalTo(newCustomerInformation));
    }

    @And("^Metoda göre log sayfasından filtreleme işlemi yapılır$")
    public void metodaGoreLogSayfasindanFiltrelemeIslemiYapilir() {
        actor.attemptsTo(Enter.theValue("update").into(ApplicationPages.LogPage.methodName));
    }
}
