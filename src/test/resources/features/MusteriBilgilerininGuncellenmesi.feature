@regression
Feature: Bir müşterinin bilgilerinin güncellenmesi

  Scenario Outline: Bir müşterinin bilgileri güncellenir
    Given Kullanıcı PoC2 anasayfasını açar
    And Customer Sekmesine Tıklanır
    And <Tckn> ait müşteri düzenlenir
    And Müşterinin <Mail Adresi>, <Telefon>, <Ülke>, <Şehir>, <Cadde> ve posta kodu düzenlenir
    And Save Butonuna Tıklanır
    And Log Sekmesine Tıklanır
    And Metoda göre log sayfasından filtreleme işlemi yapılır
    And Search Butonuna Tıklanır
    And Log sayfasından müşteri bilgilerinin düzenlendiği görülür

    Examples:
      | Tckn          | Mail Adresi   | Telefon | Ülke   | Şehir  | Cadde  |
      | 1231231232112 | test@test.com | 0000002 | Deneme | Deneme | Deneme |