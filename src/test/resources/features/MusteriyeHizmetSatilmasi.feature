@regression
Feature: Bir customer'a katalogtaki bir fiber hizmeti satılması

  Scenario Outline: Bir müşteriye katalogtaki bir fiber hizmeti satılır
    Given Kullanıcı PoC anasayfasını açar
    And Order Sekmesine Tıklanır
    And Add Butonuna Tıklanır
    And <TCKN> kimlik numaralı müşterinin önceki siparişleri tutulur
    And <Product> ürünü seçilir
    And <Customer> müşterisi seçilir
    And Save Butonuna Tıklanır
    And Çıkan uyarıda OK butonuna basılır
    Then Müşterinin almış olduğu bu ürün verisi sorgulanır ve detayları dönülür

    Examples:
      | Product | Customer | TCKN        |
      | Test    | Test     | 11111111111 |